from django.urls import include, path
from . import views
from .views import NewsListView, LanguageAzListView, LanguageRuListView, LanguageEnListView, EmailListView, CreatePost

urlpatterns = [
    path('', views.index, name='index'),
    path('news', NewsListView.as_view(), name='news'),
    path('azlanguage', LanguageAzListView.as_view(), name='language'),
    path('rulanguage', LanguageRuListView.as_view(), name='language'),
    path('enlanguage', LanguageEnListView.as_view(), name='language'),
    # path('emails', views.post, name="post"),
    path('emails', views.post, name='post'),
    # path('emails', views.CreatePost.as_view()),

]