""" DJANGO MODELS Import
"""
from django.db import models

#DJANGO MODELS CREATE

#News model
class News(models.Model):
    newsTitle = models.CharField(max_length=40)
    newsDescription = models.TextField()
    newsMedia = models.FileField(blank=True, null=True, upload_to="images/")
    newsDate = models.DateTimeField(auto_now_add=True, blank=True)
    def __str__(self):
        return self.newsTitle

#labguages model
class LanguageAz(models.Model):
    key = models.CharField(max_length=50)
    value = models.CharField(max_length=100)
    
    def __str__(self):
        return self.key + " - " + self.value

class LanguageRu(models.Model):
    key = models.CharField(max_length=50)
    value = models.CharField(max_length=100)
    
    def __str__(self):
        return self.key + " - " + self.value
class LanguageEn(models.Model):
    key = models.CharField(max_length=50)
    value = models.CharField(max_length=100)
    
    def __str__(self):
        return self.key + " - " + self.value




class Email(models.Model):
    name = models.CharField(max_length=200)
    email = models.CharField(max_length=200)
    title = models.CharField(max_length=200)
    text = models.TextField()

    def __str__(self):
        return self.title
    