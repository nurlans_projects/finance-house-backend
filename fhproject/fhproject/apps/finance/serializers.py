""" TEST """
from rest_framework import serializers

from .models import News, LanguageAz, LanguageRu, LanguageEn, Email


# News SERIALIZER
class ArticleSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=120)
    description = serializers.CharField()
    body = serializers.CharField()

class NewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = News
        fields = ('id', 'newsTitle', 'newsDescription',
                  'newsMedia', 'newsDate')

# Language Serializer


class LanguageAzSerializer(serializers.ModelSerializer):
    class Meta:
        model = LanguageAz
        fields = ('id', 'key', 'value')

class LanguageRuSerializer(serializers.ModelSerializer):
    class Meta:
        model = LanguageRu
        fields = ('id', 'key', 'value')

class LanguageEnSerializer(serializers.ModelSerializer):
    class Meta:
        model = LanguageEn
        fields = ('id', 'key', 'value')


class EmailSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'name', 'email', 'title', 'text')
        model = Email
