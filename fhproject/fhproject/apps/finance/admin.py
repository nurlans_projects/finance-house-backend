from django.contrib import admin
from .models import  News, LanguageAz, LanguageRu, LanguageEn, Email

#registered Cities and Hotel table in database
admin.site.register(News)
admin.site.register(LanguageAz)
admin.site.register(LanguageRu)
admin.site.register(LanguageEn)
admin.site.register(Email)
