from django.http import HttpResponse
from rest_framework.response import Response

from rest_framework.generics import ListAPIView, RetrieveAPIView, ListCreateAPIView
from .models import News, LanguageAz, LanguageRu, LanguageEn, Email
from .serializers import NewsSerializer, LanguageAzSerializer, LanguageRuSerializer, LanguageEnSerializer, EmailSerializer

from django.shortcuts import render
from django.core.mail import send_mail
from django.conf import settings
import json
from django.views.decorators.csrf import csrf_exempt

from rest_framework.decorators import api_view
from rest_framework.decorators import parser_classes
from rest_framework.parsers import JSONParser


def index(request):
    # return HttpResponse("Hello, world. You're at the polls index.")
    return HttpResponse("Hello")


class NewsListView(ListAPIView):
    queryset = News.objects.all().order_by("-id")
    serializer_class = NewsSerializer


class LanguageAzListView(ListAPIView):
    queryset = LanguageAz.objects.all()
    serializer_class = LanguageAzSerializer

class LanguageRuListView(ListAPIView):
    queryset = LanguageRu.objects.all()
    serializer_class = LanguageRuSerializer

class LanguageEnListView(ListAPIView):
    queryset = LanguageEn.objects.all()
    serializer_class = LanguageEnSerializer



class EmailListView(ListCreateAPIView):
    queryset = Email.objects.all()
    serializer_class = EmailSerializer


# def post(request):
#     data = request.all().order_by('id')[0]
#     name = request[0].name
#     title = request[0].title
#     text = request[0].text
#     email_from = settings.EMAIL_HOST_USER
#     email_to = [request[0].email,]
#     print("BAL")
#     return send_mail(title, text, email_from, email_to)

# from rest_framework.decorators import api_view
# from rest_framework.decorators import parser_classes
# from rest_framework.parsers import JSONParser

# @api_view(['POST']) 
# @parser_classes((JSONParser,)) 
# def post(request, format=None):
#     """
#     A view that can accept POST requests with JSON content.
#     """
#     print(request.data)
#     return HttpResponse({'received data': request.data})

@csrf_exempt
@api_view(['POST']) 
@parser_classes((JSONParser,)) 
def post(request, format=None):
    name = request.data['name']
    phone = request.data['phone']
    title = request.data['title']
    email_from = settings.EMAIL_HOST_USER
    email_to = [request.data['email'],]
    text = request.data['text']

    title_text = '<p> Göndərən şəxs: <strong>{}</strong></p> <p>Nömrəsi: <strong>{}</strong></p> <p>Mövzunun adı: <strong> {} </strong> </p> <p> <strong> Muraciət: </strong> </p> <p> {} </p>'.format(name, phone, title, text)

    send_mail(name, text, email_from, email_to, html_message=title_text)
    return Response({'Status': "ok"})




class CreatePost(ListCreateAPIView):
    queryset = Email.objects.all()
    serializer_class = EmailSerializer
   
        
    

    # def create(self):
    #     size = len(self.queryset)
    #     name = self.queryset[size-1].name
    #     title = self.queryset[size-1].title
    #     email_to = [self.queryset[size-1].email, ]
    #     email_from = settings.EMAIL_HOST_USER
    #     text  = self.queryset[size-1].text
    #     send_mail(title, name, email_from, email_to)
    #     return HttpResponse("Success")
    
    
    


# def indexSend(request):
    
# 	if request.method == 'POST':
# 		message = request.POST['message']

# 		send_mail('Contact Form',
# 		 message, 
# 		 settings.EMAIL_HOST_USER,
# 		 ['ivanovsin11@gmail.com'], 
# 		 fail_silently=False)
# 	return render(request, 'app/email.html')

# def send_email(request):
#     if request.method == 'POST':
#         subject = request.POST.get('title')
#         name = request.POST.get('name')
#         # text = request.POST['text']
#         email_from = settings.EMAIL_HOST_USER
#         recipient_list = request.POST.get('email')
#         # recipient_list = ['noor.mammadov@gmail.com',]
#         send_mail(subject, name, email_from, recipient_list)
#     return  HttpResponse("Success")


